//
// Created by pwit on 08.07.17.
//

#include<memory>
#include <gtest/gtest.h>
#include "classes.h"


using namespace std;

TEST(MovieTest, basic) {

    std::string hro_title = "The Hunt for Red October";

    Movie hro(hro_title);

    EXPECT_EQ(hro.getTitle(), hro_title);
    EXPECT_EQ(hro.getPriceCode(), Movie::PriceCode::REGULAR);

    hro.setPriceCode(Movie::PriceCode::NEW_RELEASE);

    EXPECT_EQ(hro.getTitle(), hro_title);
    EXPECT_EQ(hro.getPriceCode(), Movie::PriceCode::NEW_RELEASE);

}

TEST(RentalTest, basic) {

    std::string hro_title = "The Hunt for Red October";
    Movie hro(hro_title);

    Rental hro_rent(hro, 44);

    EXPECT_EQ(hro_rent.getMovie(), hro);
    EXPECT_EQ(hro_rent.getDaysRented(), 44);
}


TEST(CustomerTest, basic_regular_short) {
    Customer c("John Smith");

    Movie hfro("The Hunt for Red October");
    hfro.setPriceCode(Movie::PriceCode::REGULAR);
    Rental hfro_reg_2(hfro, 2);

    c.addRental(hfro_reg_2);

    std::string expected = "Rental Record for John Smith\n"
            "\tThe Hunt for Red October\t2.000000\n"
            "Amount owed is 2.000000\nYou earned 1 frequent renter points";

    string result = c.statement();

    EXPECT_EQ(expected, result);
}


TEST(CustomerTest, basic_regular_long) {
    Customer c("John Smith");

    Movie hfro("The Hunt for Red October");
    hfro.setPriceCode(Movie::PriceCode::REGULAR);
    Rental hfro_reg_44(hfro, 44);

    c.addRental(hfro_reg_44);

    std::string expected = "Rental Record for John Smith\n"
            "\tThe Hunt for Red October\t65.000000\n"
            "Amount owed is 65.000000\nYou earned 1 frequent renter points";

    string result = c.statement();

    EXPECT_EQ(expected, result);
}


TEST(CustomerTest, basic_new) {
    Customer c("John Smith");

    Movie bbok("Black Book");
    bbok.setPriceCode(Movie::PriceCode::NEW_RELEASE);
    Rental bbok_new_2(bbok, 2);

    c.addRental(bbok_new_2);

    std::string expected = "Rental Record for John Smith\n"
            "\tBlack Book\t6.000000\n"
            "Amount owed is 6.000000\nYou earned 2 frequent renter points";


    string result = c.statement();

    EXPECT_EQ(expected, result);
}

TEST(CustomerTest, basic_child_short) {
    Customer c("John Smith");


    Movie htf("Happy Tree Friends");
    htf.setPriceCode(Movie::PriceCode::CHILDRENS);
    Rental htf_ch_3(htf, 3);

    c.addRental(htf_ch_3);

    std::string expected = "Rental Record for John Smith\n"
            "\tHappy Tree Friends\t1.500000\n"
            "Amount owed is 1.500000\nYou earned 1 frequent renter points";


    string result = c.statement();

    EXPECT_EQ(expected, result);
}


TEST(CustomerTest, basic_child_long) {
    Customer c("John Smith");


    Movie htf("Happy Tree Friends");
    htf.setPriceCode(Movie::PriceCode::CHILDRENS);
    Rental htf_ch_30(htf, 30);

    c.addRental(htf_ch_30);

    std::string expected = "Rental Record for John Smith\n"
            "\tHappy Tree Friends\t42.000000\n"
            "Amount owed is 42.000000\nYou earned 1 frequent renter points";


    string result = c.statement();

    EXPECT_EQ(expected, result);
}


TEST(CustomerTest, complex1) {

    Customer c("John Smith");

    Movie hfro("The Hunt for Red October");
    Rental hfro_reg_44(hfro, 44);

    c.addRental(hfro_reg_44);

    std::string expected = "Rental Record for John Smith\n"
            "\tThe Hunt for Red October\t65.000000\n"
            "Amount owed is 65.000000\nYou earned 1 frequent renter points";

    string result = c.statement();

    EXPECT_EQ(expected, result);

    Movie bbok("Black Book");
    bbok.setPriceCode(Movie::PriceCode::NEW_RELEASE);
    Rental bbok_new_2(bbok, 2);

    c.addRental(bbok_new_2);

    expected = "Rental Record for John Smith\n"
            "\tThe Hunt for Red October\t65.000000\n"
            "\tBlack Book\t6.000000\n"
            "Amount owed is 71.000000\nYou earned 3 frequent renter points";


    result = c.statement();

    EXPECT_EQ(expected, result);

    Movie htf("Happy Tree Friends");
    htf.setPriceCode(Movie::PriceCode::CHILDRENS);
    Rental htf_ch_30(htf, 30);

    c.addRental(htf_ch_30);

    expected = "Rental Record for John Smith\n"
            "\tThe Hunt for Red October\t65.000000\n"
            "\tBlack Book\t6.000000\n"
            "\tHappy Tree Friends\t42.000000\n"
            "Amount owed is 113.000000\nYou earned 4 frequent renter points";

    result = c.statement();

    EXPECT_EQ(expected, result);
}

