//
// Created by pwit on 08.07.17.
//
//
#ifndef REFEX_CLASSES_H
#define REFEX_CLASSES_H

#include <string>
#include <vector>

#include <iostream>

class Movie {

public:
    enum class PriceCode {
        REGULAR = 0, NEW_RELEASE = 1, CHILDRENS = 2
    };
private:
    std::string _title;
    PriceCode _priceCode;

    typedef double (*chargeFunction)(unsigned);

    static double getChargeRegular(unsigned days) {
        return 2 + (days > 2 ? (days - 2) * 1.5 : 0);
    }

    static double getChargeNew_Release(unsigned days) {
        return days * 3;
    }

    static double getChargeChildren(unsigned days) {
        return 1.5 + (days > 3 ? (days - 3) * 1.5 : 0);
    }

    chargeFunction chargeFunctions[3] = {getChargeRegular, getChargeNew_Release, getChargeChildren};

public:

    Movie(std::string title) : _title(title), _priceCode(PriceCode::REGULAR) {}

    PriceCode getPriceCode() {
        return _priceCode;
    }

    void setPriceCode(PriceCode arg) {
        _priceCode = arg;
    }

    std::string getTitle() {
        return _title;
    }

    bool operator==(const Movie &rhs) const {
        return _title == rhs._title &&
               _priceCode == rhs._priceCode;
    }

    bool operator!=(const Movie &rhs) const {
        return !(rhs == *this);
    }

    double getAmount(unsigned days) {
        return chargeFunctions[static_cast<unsigned>(_priceCode)](days);
    }


};

class Rental {
private:
    Movie _movie;
    unsigned _daysRented;

public:
    Rental(Movie movie, unsigned daysRented) : _movie(movie),
                                               _daysRented(daysRented) {}

    unsigned getDaysRented() {
        return _daysRented;
    }

    Movie getMovie() {
        return _movie;
    }

    double getAmount() {
        return getMovie().getAmount(getDaysRented());
    }


    int getRenterPoints() {
        int frequentRenterPoints = 1;

// add bonus for a two day new release rental
        if ((getMovie().getPriceCode() == Movie::PriceCode::NEW_RELEASE)
            && getDaysRented() > 1)
            frequentRenterPoints++;

        return frequentRenterPoints;
    }

};


class Customer {
private:
    std::string _name;
    std::vector<Rental> _rentals;

    std::string addHeader() {
        return "Rental Record for " + getName() + "\n";
    }

    std::string addFigures(double thisAmount, Rental &rental) const {
        return "\t"
               + rental.getMovie().getTitle() + "\t" +
               std::to_string(thisAmount)
               + "\n";
    }

    std::string addFooter(double totalAmount, int frequentRenterPoints) const {
        return "Amount owed is " + std::to_string(totalAmount) +
               "\n" +
               "You earned " + std::to_string(frequentRenterPoints)
               +
               " frequent renter points";
    }


public:
    Customer(std::string name) : _name(name) {};

    void addRental(Rental arg) {
        _rentals.push_back(arg);
    }

    std::string getName() {
        return _name;
    }


    std::string statement() {
        double totalAmount = 0;
        int frequentRenterPoints = 0;
        double thisAmount;

        std::string result = addHeader();

        for (auto rental : _rentals) {
//determine amounts for rental line
            thisAmount = rental.getAmount();
// add frequent renter points
            frequentRenterPoints += rental.getRenterPoints();
//show figures for this rental
            result += addFigures(thisAmount, rental);
            totalAmount += thisAmount;
        }
//add footer lines
        result += addFooter(totalAmount, frequentRenterPoints);
        return result;
    }

};


#endif //REFEX_CLASSES_H
