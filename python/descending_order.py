# Your task is to make a function that can take any non-negative integer as a argument and return it with it's digits
#  in descending order. Essentially, rearrange the digits to create the highest possible number.
#
# Examples:
#
# Input: 21445 Output: 54421
# Input: 145263 Output: 654321
# Input: 1254859723 Output: 9875543221
#
# FUNDAMENTALS, FUNCTIONS, CONTROL FLOW, BASIC LANGUAGE FEATURES


from functools import reduce

def Descending_Order(num):
    tbl = sorted(list(str(num)), reverse = True)
    return reduce(lambda num,digit : 10*num + int(digit),tbl, 0)

from nose.tools import *

assert_equals(Descending_Order(0), 0)
assert_equals(Descending_Order(15), 51)
assert_equals(Descending_Order(123456789), 987654321)