# Create a function named divisors that takes an integer and returns an array with all of the integer's
# divisors(except for 1 and the number itself). If the number is prime return the string '(integer) is prime'.
#
# Example:
#
# divisors(12); #should return [2,3,4,6]
# divisors(25); #should return [5]
# divisors(13); #should return "13 is prime"
# You can assume that you will only get positive integers as inputs.
# ALGORITHMS MATHEMATICS NUMBERS


def divisors(number):
    result = []
    for i in range(2, number):
        if number % i == 0:
            result.append(i)

    return str(number) + " is prime" if result == [] else result


from nose.tools import *

assert_equals(divisors(15), [3, 5]);
assert_equals(divisors(12), [2, 3, 4, 6]);
assert_equals(divisors(13), "13 is prime");
